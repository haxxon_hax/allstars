<!DOCTYPE HTML>  
<html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width">
  <title>East Mesa Karate All Stars</title>
  <link rel="stylesheet" href="css/style.css">
<style>
.error {color: #FF0000;}
</style>
</head>
<body>  

<?php
// define variables and set to empty values
$success = FALSE;
// We need a variable to say everything is good at the end.
$good = TRUE;

$firstnameErr = $lastnameErr = $allstarErr = $emailErr = $cellErr = "";
$firstname = $lastname = $allstar = $email = $cell = $cellNormal = "";
$emailNotice = $prefEmail = $prefText = "off";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (empty($_POST["firstname"])) {
    $firstnameErr = "First name is required";
  } else {
    $firstname = test_input($_POST["firstname"]);
    // check if name only contains letters and whitespace
    if (!preg_match("/^[a-zA-Z]*$/",$firstname)) {
      $firstnameErr = "Only letters are allowed"; 
    }
  }
  
  if (empty($_POST["lastname"])) {
    $lastnameErr = "Last name is required";
  } else {
    $lastname = test_input($_POST["lastname"]);
    // check if name only contains letters and whitespace
    if (!preg_match("/^[a-zA-Z-]*$/",$lastname)) {
      $lastnameErr = "Only letters and dashes are allowed"; 
    }
  }
  
  if (empty($_POST["allstar"])) {
    $allstarErr = "All Star name is required";
  } else {
    $allstar = test_input($_POST["allstar"]);
    // check if name only contains letters and whitespace
    if (!preg_match("/^[a-zA-Z -]*$/",$allstar)) {
      $allstarErr = "Only letters, spaces, and dashes are allowed"; 
    }
  }
  
  if (empty($_POST["email"])) {
    $emailErr = "Email is required";
  } else {
    $email = test_input($_POST["email"]);
    // check if e-mail address is well-formed
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $emailErr = "Invalid email format"; 
    }
  }
    
  if (empty($_POST["cell"])) {
    $cellErr = "Phone is required";
  } else {
    $cell = test_input($cell);
    $cell = format_phone_us($_POST["cell"]);
    $cellNormal = preg_replace('/[^0-9.]+/', '', $cell);
    // check if cell number is well-formed
    if (!preg_match("/^\([0-9][0-9][0-9]\) [0-9][0-9][0-9]-[0-9][0-9][0-9][0-9]$/",$cell)) {
      $cellErr = "Invalid phone format"; 
    }
  }

  if (!empty($_POST["prefemail"])){
    $prefEmail = $_POST["prefemail"];
    $emailNotice = $prefEmail;
  }
  if (!empty($_POST["preftext"])){
    $prefText = $_POST["preftext"];
  }
    
  if (($firstnameErr . $lastnameErr . $allstarErr . $emailErr . $cellErr) != "")
  {
    $success = FALSE;
  } else
  {
    $success = TRUE;
  }

}

if ( $success === FALSE )
{
  include 'form.php';
}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

function format_phone($country, $phone) {
  $function = 'format_phone_' . $country;
  if(function_exists($function)) {
    return $function($phone);
  }
  return $phone;
}

function format_phone_us($phone) {
  // note: making sure we have something
  if(!isset($phone{3})) { return ''; }
  // note: strip out everything but numbers 
  $phone = preg_replace("/[^0-9]/", "", $phone);
  $length = strlen($phone);
  switch($length) {
  case 7:
    return preg_replace("/([0-9]{3})([0-9]{4})/", "$1-$2", $phone);
  break;
  case 10:
   return preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "($1) $2-$3", $phone);
  break;
  case 11:
  return preg_replace("/([0-9]{1})([0-9]{3})([0-9]{3})([0-9]{4})/", "$1($2) $3-$4", $phone);
  break;
  default:
    return $phone;
  break;
  }
}

function print_submitted_data($firstname,$lastname,$email,$cell,$allstar,$prefEmail,$prefText) {
  // Once we've got here, everything is hunky-dory.
  echo "<div class=\"output_box\">";
  echo "<h2>Your Input:</h2>";
  echo "<a href='https://www.facebook.com/groups/eastmesaallstars/'>CLICK HERE to JOIN OUR FACEBOOK GROUP</a>";
  echo "<table>";
  echo "<tr>";
  echo "<td>First Name:</td>";
  echo "<td>" . $firstname . "</td>";
  echo "</tr>";
  echo "<tr>";
  echo "<td>Last Name:</td>";
  echo "<td>" . $lastname . "</td>";
  echo "</tr>";
  echo "<tr>";
  echo "<td>Email:</td>";
  echo "<td>" . $email . "</td>";
  echo "</tr>";
  echo "<tr>";
  echo "<td>Cell:</td>";
  echo "<td>" . $cell . "</td>";
  echo "</tr>";
  echo "<tr>";
  echo "<td>Cell:</td>";
  echo "<td>" . $cell . "</td>";
  echo "</tr>";
  echo "<tr>";
  echo "<td>All Star:</td>";
  echo "<td>" . $allstar . "</td>";
  echo "</tr>";
  echo "<tr>";
  echo "<td>Contact by Email:</td>";
  echo "<td>" . $prefEmail . "</td>";
  echo "</tr>";
  echo "<tr>";
  echo "<td>Contact by Text:</td>";
  echo "<td>" . $prefText . "</td>";
  echo "</tr>";
}

function db_connect() {
  require_once('config.php');
  //Connect to mysql server
  $link = mysql_connect(DB_HOST, DB_USER, DB_PASSWORD);
  if(!$link) {
    die('Failed to connect to server: ' . mysql_error());
  }
  
  //Select database
  $db = mysql_select_db(DB_DATABASE);
  if(!$db) {
    die("Unable to select database");
  } else {
    return $link;
  }
}

function check_for_email_in_database($email) {
  $link = db_connect();

  $qry="SELECT * FROM members WHERE email='$email';";
  $result=mysql_query($qry);

  if ($result) {
    if(mysql_num_rows($result) == 1) {
      // User exists in Database
      $emailErr = "Email is already registered."; 
      $good = FALSE;
    }
  }

  mysql_close($link);
}

function remove_recipient_from_email($sparkpost_contacts,$firstname,$lastname,$allstar,$cell,$email) {
  $newrecipients = array();
  for ($i = 0; $i < count($sparkpost_contacts["results"]["recipients"]); ++$i) {
    unset($sparkpost_contacts["results"]["recipients"][$i]["return_path"]);
  }
  for ($i = 0; $i < count($sparkpost_contacts["results"]["recipients"]); ++$i) {
    echo "Comparing " . $sparkpost_contacts["results"]["recipients"][$i]["address"]["email"] . " with " . $email . ".";
    if ($sparkpost_contacts["results"]["recipients"][$i]["address"]["email"] === $email) {
      $deleteme = $i;
    } else {
      array_push($newrecipients,$sparkpost_contacts["results"]["recipients"]);
    }
  }
  unset($sparkpost_contacts["results"]["recipients"][$deleteme]);
  unset($sparkpost_contacts["results"]["total_accepted_recipients"]);
  unset($sparkpost_contacts["results"]["id"]);
  // var_dump(json_encode($sparkpost_contacts["results"]));
  $sparkpost_encoded=json_encode($sparkpost_contacts["results"]);

  // Check if email is in the system
  $myurl = "";
  $myurl = "https://api.sparkpost.com/api/v1/recipient-lists/east-mesa-karate-all-stars?num_rcpt_errors=3";
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  // Will return the response, if false it print the response
  curl_setopt($ch, CURLOPT_POST, true);
  // Will return the response, if false it print the response
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  // Set the url
  curl_setopt($ch, CURLOPT_URL,$myurl);
  // Set the user passwd
  curl_setopt($ch, CURLOPT_USERPWD,'4424fca01f944d2c13738a28e41e365b1e019325');

  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
  curl_setopt($ch, CURLOPT_POSTFIELDS, $sparkpost_encoded);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
    'Content-Type: application/json',                                                                                
    'Content-Length: ' . strlen($sparkpost_encoded))                                                                       
  );
   
  // Execute
  $sparkpost_return = curl_exec($ch);
  // var_dump($sparkpost_return);
  // Closing
  curl_close($ch);
}

function add_recipient_to_email($sparkpost_contacts,$firstname,$lastname,$allstar,$cell,$email) {
  $recipient = array();
  $recipient["address"] = array();
  $recipient["address"]["email"] = $email;
  $recipient["address"]["name"] = $firstname . " " . $lastname;
  for ($i = 0; $i < count($sparkpost_contacts["results"]["recipients"]); ++$i) {
    unset($sparkpost_contacts["results"]["recipients"][$i]["return_path"]);
  }
  array_push($sparkpost_contacts["results"]["recipients"],$recipient);

  unset($sparkpost_contacts["results"]["total_accepted_recipients"]);
  unset($sparkpost_contacts["results"]["id"]);
  // var_dump(json_encode($sparkpost_contacts["results"]));
  $sparkpost_encoded=json_encode($sparkpost_contacts["results"]);

  // Check if email is in the system
  $myurl = "";
  $myurl = "https://api.sparkpost.com/api/v1/recipient-lists/east-mesa-karate-all-stars?num_rcpt_errors=3";
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  // Will return the response, if false it print the response
  curl_setopt($ch, CURLOPT_POST, true);
  // Will return the response, if false it print the response
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  // Set the url
  curl_setopt($ch, CURLOPT_URL,$myurl);
  // Set the user passwd
  curl_setopt($ch, CURLOPT_USERPWD,'4424fca01f944d2c13738a28e41e365b1e019325');

  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
  curl_setopt($ch, CURLOPT_POSTFIELDS, $sparkpost_encoded);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
    'Content-Type: application/json',                                                                                
    'Content-Length: ' . strlen($sparkpost_encoded))                                                                       
  );
   
  // Execute
  $sparkpost_return = curl_exec($ch);
  // var_dump($sparkpost_return);
  // Closing
  curl_close($ch);
}

function update_member_in_database($firstname,$lastname,$allstar,$cell,$email,$prefEmail,$prefText) {
  $link = db_connect();
  $eflag = 0;
  $tflag = 0;
  $encpwd = md5('autogenerated');

  if ($prefEmail == 'on') { $eflag = 1; }
  if ($prefText == 'on') { $tflag = 1; }

  $qry="SELECT * FROM members WHERE email='$email';";
  $result=mysql_query($qry);

  if ($result) {
    if(mysql_num_rows($result) == 1) {
      // User exists in Database
      $action = "UPDATE `members` SET firstname='$firstname',lastname='$lastname',allstar='$allstar',phone='$cell',email='$email',prefemail=$eflag,preftext=$tflag WHERE email='$email';";
    } else {
      $action = "INSERT INTO `members` (login,passwd,firstname,lastname,allstar,phone,email,prefemail,preftext) VALUES ('$email','$encpwd','$firstname','$lastname','$allstar','$cell','$email',$eflag,$tflag);";
    }
  }

  print '<script type="javascript">console.log("' . $action . '");</script>';
  $r=mysql_query($action);
  if (!$r) {
    $firstnameErr = "Error adding to the Database!";
    $good = FALSE;
  } else {
    $good = TRUE;
  }
  mysql_close($link);
}

if ($success === TRUE) {
  # TODO: Add get recipient list
  # https://developers.sparkpost.com/api/recipient-lists.html#recipient-lists-retrieve-get

  # GET /api/v1/recipient-lists/unique_id_4_graduate_students?show_recipients=true


  # TODO: Add update recipient list
  # https://developers.sparkpost.com/api/recipient-lists.html#recipient-lists-update-put

  # PUT/api/v1/recipient-lists/{id}{?num_rcpt_errors}

  // https://api.sparkpost.com/api/v1/sending-domains/something-magical.net/verify

  // Check if email is in the system
  $isinsystem = FALSE;
  $myurl = "";
  $myurl = "https://api.sparkpost.com/api/v1/recipient-lists/east-mesa-karate-all-stars?show_recipients=true";
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  // Will return the response, if false it print the response
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  // Set the url
  curl_setopt($ch, CURLOPT_URL,$myurl);
  // Set the user passwd
  curl_setopt($ch, CURLOPT_USERPWD,'4424fca01f944d2c13738a28e41e365b1e019325');
  // Execute
  $sparkpost_contacts = curl_exec($ch);
  // Closing
  curl_close($ch);

  // var_dump($sparkpost_contacts);
  $sparkpost_contacts = json_decode($sparkpost_contacts, true);

  foreach ($sparkpost_contacts["results"]["recipients"] as $recip) {
    if($recip["address"]["email"] === $email) { $isinsystem = TRUE; }
  }

  // If they've checked prefer email...
  if ($prefEmail === "on")
  {
   // TODO
    // Instead of erroring and failing, how about we just ignore and press on?
    if ($isinsystem === TRUE) {
      $emailNotice = "Email is already subscribed."; 
    } else {
      // create array
      add_recipient_to_email($sparkpost_contacts,$firstname,$lastname,$allstar,$cell,$email);
    }
  } else {
    // If they've unchecked it, we should check to see if they're in there, then remove them.
    if ($isinsystem === FALSE) {
      // They're not in the system.
      $emailNotice = "Email is already unsubscribed.";
    } else {
      remove_recipient_from_email($sparkpost_contacts,$firstname,$lastname,$allstar,$cell,$email);
    }
  }

  // Check if email and details are in database
  // If not, save to database.

  print '<script type="javascript">console.log("Finished subscription check.");</script>';
  if ($good === TRUE) {
    print '<script type="javascript">console.log("Good was true.");</script>';
    // Checks are good
    update_member_in_database($firstname,$lastname,$allstar,$cellNormal,$email,$prefEmail,$prefText);
    if ($good === TRUE) {
      print_submitted_data($firstname,$lastname,$email,$cell,$allstar,$emailNotice,$prefText);
    } else {
      print '<script type="javascript">console.log("DB Error.");</script>';
      include 'form.php';
    }
  } else {
      print '<script type="javascript">console.log("Error somewhere.");</script>';
      include 'form.php';
  }

  // echo "</table>";
  // echo "</div>";
  // TODO: Add Captcha
}
?>

</body>
</html>