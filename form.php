<script type="text/javascript">
  function confirm_submit() {
    var firstname = document.forms["contactinfo"]["firstname"].value;
    var lastname = document.forms["contactinfo"]["lastname"].value;
    var allstar = document.forms["contactinfo"]["allstar"].value;
    var email = document.forms["contactinfo"]["email"].value;
    var cell = document.forms["contactinfo"]["cell"].value;
    var prefemail = "";
    var preftext = "";
    if (document.forms["contactinfo"]["prefemail"].checked) {
      prefemail = "on";
    } else {
      prefemail = "off";
    }
    if (document.forms["contactinfo"]["preftext"].checked) {
      preftext = "on";
    } else {
      preftext = "off";
    }
    var text = "";
    text += "First Name: " + firstname + "\n";
    text += "Last Name: " + lastname + "\n";
    text += "All Star: " + allstar + "\n";
    text += "Email: " + email + "\n";
    text += "Cell Phone: " + cell + "\n";
    text += "Contact by Email: " + prefemail + "\n";
    text += "Contact by Text: " + preftext + "\n";
    text += "Click OK to Confirm:";
    return confirm(text);
  }
</script>

<form method="post" name="contactinfo" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" onsubmit="return confirm_submit();">
  <header>East Mesa Karate All Stars</header>
  <label class="text_label" for="firstname">First Name<span class="error">* <?php echo $firstnameErr;?></span></label> <input type="text" id="firstname" name="firstname" placeholder="Your First Name" value="<?php echo $firstname;?>" required /><br />
  <label class="text_label" for="lastname">Last Name<span class="error">* <?php echo $lastnameErr;?></span></label> <input type="text" id="lastname" name="lastname" placeholder="Your First Name" value="<?php echo $lastname;?>" required /><br />

  <label class="text_label">All Star's Name<span class="error">* <?php echo $allstarErr;?></span></label>
  <input type="text" id="allstar" name="allstar" placeholder="The Name of the All Star" value="<?php echo $allstar;?>" required /><br />

  <label class="text_label">Email<span class="error">* <?php echo $emailErr;?></span></label>
  <input type="text" id="email" name="email" placeholder="name@something.com" value="<?php echo $email;?>" required /><br />

  <label class="text_label">Cell Phone<span class="error">* <?php echo $cellErr;?></span></label>
  <input type="text" id="cell" name="cell" placeholder="(123) 456-7890" value="<?php echo $cell;?>" required /><br />
  <p>
  <input type=checkbox id="prefemail" name="prefemail" <?php if ($prefEmail == "on") print "checked";?> />
  <label for="prefemail">Check here for email notifications.</label>
  </p>
  <p>
  <input type=checkbox id="preftext" name="preftext" <?php if ($prefText == "on") print "checked";?> /><label for="preftext">Check here for Text/SMS notifications.</label>
  </p>
  
  <input type="submit" name="submit" value="Submit">
</form>